#include <stdio.h>
//MPI
#include <mpi.h>
void Get_input(int my_rank, int comm_sz, int* n, int* count, int* i);
double My_funcion(double res, double a, double b,int n, int count, int i);
int main(void)
{
    int my_rank, comm_sz, n, count, i;
    double local_res, total_res;
    double res=0;
    double a=0;
    double b=0;
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    Get_input(my_rank, comm_sz, &n, &count, &i);
    local_res= My_funcion(res,a,b,&n,&count,&i);
    MPI_Reduce(&local_res, &total_res, 1, MPI_DOUBLE, MPI_SUM,0,MPI_COMM_WORLD);

    if(my_rank==0){
        printf("Con el valor n= %d el resultado es: %f \n",n,total_res);
    }
    MPI_Finalize();
    return 0;
}

void Get_input(int my_rank, int comm_sz, int* n, int* count, int* i){
    if (my_rank ==0){
        printf("Ingrese los valores de n, count y i\n");
        scanf("%d %d %d", n, count, i);
    }
    MPI_Bcast(n,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(count,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(i,1,MPI_INT,0,MPI_COMM_WORLD);
}

double My_funcion(
    double res,
    double a,
    double b,
    int n,
    int count,
    int i
){
    while (i<n){
        a= rand();
        b=rand();
        if((a*a)+(b*b)<1){
            count++;
        }
            i++;
    }
    res= (4*((double)count/n));
    return res;
}
